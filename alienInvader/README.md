# Access the project folder in your terminal
$ cd project/files

# Install the dependencies
$ npm install -g http-server

# Run the application in development mode
$ http-server

# The application will open on the port: 3000 - go to http://localhost:3000
