public class Main {
    public static void main(String[] args) throws Exception {
        Funcionario f1 = new Funcionario("F", 10.0, 10, "Julia", "M", 1, 10.0);
        Funcionario f2 = new Funcionario("M", 10.0, 10, "Henrique", "N", 2, 10.0);
        System.out.println(f1.calcularSalarioFinal());
        System.out.println(f2.calcularSalarioFinal());
    }
}
