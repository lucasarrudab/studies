public class Funcionario {
    private String sexo;
    private double valorHorasTrabalhadas;
    private int quantidadeHorasTrabalhadas;
    private String nome; 
    private String turnoTrabalho;
    private int cpf;
    private double salario;

   //Constructors

    public Funcionario(){
        this.sexo = "M"; 
        this.valorHorasTrabalhadas = 0.0;
        this.quantidadeHorasTrabalhadas = 0;
        this.nome = "Sem nome";
        this.turnoTrabalho = "M";
        this.cpf = 0;
        this.salario = 0;
    }

    public Funcionario(String sexo, double vht, int qht, String nome, String tt, int cpf, double salario){
            this.sexo = sexo; 
            this.valorHorasTrabalhadas = vht;
            this.quantidadeHorasTrabalhadas = qht;
            this.nome = nome;
            this.turnoTrabalho = tt;
            this.cpf = cpf;
            this.salario = salario;
    }

    //Setters

    public void setSexo(String sexo){
        this.sexo = sexo;
    }

    public void setValorHorasTrabalhadas(double vht){
        this.valorHorasTrabalhadas = vht;
    }

    public void setQuantidadeHorasTrabalhadas(int qht){
        this.quantidadeHorasTrabalhadas = qht;
    }

    public void setNome(String nome){
       this.nome = nome;
    }

    public void setTurnoTrabalho(String tt){
        this.turnoTrabalho = tt;
    }

    public void setCpf(int cpf){
        this.cpf = cpf;
    }

    //Getters

    public String getSexo(){
        return this.sexo;
    }

    public double getValorHorasTrabalhadas(){
        return this.valorHorasTrabalhadas;
    }

    public int getQuantidadeHorasTrabalhadas(){
        return this.quantidadeHorasTrabalhadas;
    }

    public String getNome(){
       return this.nome;
    }

    public String getTurnoTrabalho(){
        return this.turnoTrabalho;
    }

    public int getCpf(){
        return this.cpf;
    }

    public double calcularSalarioFinal(){
        if(sexo == "F"){
            this.salario = (valorHorasTrabalhadas * quantidadeHorasTrabalhadas) * 1.20;
        } else {
            this.salario = valorHorasTrabalhadas * quantidadeHorasTrabalhadas;
        }
            return this.salario;

    }

    public double calcularSalarioAnual(){
        return calcularSalarioFinal() * 12;
    }

    public String alterarTurno(){
        if(this.turnoTrabalho == "M"){
            this.turnoTrabalho = "N";
        } else if(this.turnoTrabalho == "N"){
            this.turnoTrabalho = "M";
        }
        return this.turnoTrabalho;

    }

    

}
