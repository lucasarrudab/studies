public class Invoice{
    private String iden;
    private int quantia;
    private double preco;


    public Invoice(String iden, int quantia, double preco){
        this.iden = iden;
        if(quantia < 0){
            this.quantia = 0;
        }else{
            this.quantia = quantia;
        }
        if(preco < 0){
            this.preco = 0;
        } else{
            this.preco = preco;
        }
    }

    public double calcularFatura(){
        return this.preco * this.quantia;
    }

    public void setIden(String iden){
        this.iden = iden;
    }

    public void setQuantia(int quantia){
        this.quantia = quantia;
    }

    public void setPreco(double preco){
        this.preco = preco;
    }

    public String getIden(){
        return this.iden;
    }

    public int getQuantia(){
        return this.quantia;
    }

    public double getPreco(){
        return this.preco;
    }

}