public class Main {
    public static void main(String[] args) throws Exception {
        
    // Classes - Constutores e encapsulamento PT 1

        // p1.altura = 1.70;
        // p1.peso = 100;
        // p1.aguaPorKg  = 0.035;
        // p1.anoAtual = 2024;
        // p1.anoNascimento = 1998;
        
        // Pessoa p2 = new Pessoa(1.79, 80.0, 1995, "Joaquim");
        // p2.aguaPorKg  = 0.035;
        // p2.anoAtual = 2024;
        // p2.mostrar();
        
    // Classes - Atributos e metodos PT 2 Exercicio 1 a 3

        // Conta c1 = new Conta();
        // Conta c2 = new Conta();
        
        // c1.cpf = 1;
        // c1.numConta = 1;
        // c1.saldo = 100;
        // c1.clienteEspecial = true;
        
        // c2.cpf = 2;
        // c2.numConta = 2;
        // c2.saldo = 200;
        // c2.clienteEspecial = false;
        
        // c1.transferencia(c2, 100);
        // c1.bonificacao(); 
        
    // Classes - Constutores e encapsulamento PT 1 Exercicio 1

        // Pessoa p1 = new Pessoa();
        // p1.setNome("pedrinho");
        // p1.setAltura(1.70);
        // p1.setCpf(23232);
        // p1.setAnoNascimento(2000);
        // p1.mostrar();


    // Classes - Constutores e encapsulamento PT 1 Exericicio 2
        
        // Empregado e1 = new Empregado("pedro", "santana", 1000.0);
        // Empregado e2 = new Empregado("gui", "santos", -100.0);
        // System.out.println(e1.calcularSalarioAnual());
        // System.out.println(e2.calcularSalarioAnual());

    // Classes - Constutores e encapsulamento PT 1 Exercicio 3

        // Invoice in = new Invoice(20, 2.00, "Notebook");
        // in.calcularValorFatura();


     // Classes - Constutores e encapsulamento PT 2

        // Ponto2D p1 = new Ponto2D();
        // Ponto2D p2 = new Ponto2D();
        // p1.setX(3);
        // p1.setY(2);
        // p2.setX(10);
        // p2.setY(12);
        // System.out.println("A distancia e " + p1.calcularEuclidiana(p2));

        }
}
