public class Ponto2D {
    private int x;
    private int y;

    public void setX(int x){
        this.x = x;
    }

    public void setY(int y){
        this.y = y;
    }

    public int getX(){
        return this.x;
    }

    public int getY(){
        return this.y;
    }

    public boolean xEIgualY(){
        if (this.x == this.y){
            return true;
        } else {
            return false;
        }
    }

    public double calcularEuclidiana(Ponto2D outroPonto2d){
        int deltaX = this.x = outroPonto2d.getX();
        int deltaY = this.y = outroPonto2d.getY();
        return Math.sqrt(deltaX * deltaX + deltaY * deltaY);
    }
}
