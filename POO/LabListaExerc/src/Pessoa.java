public class Pessoa {
    private String nome;
    private int anoNascimento;
    private double altura;
    private int cpf;
    public  double peso;

    public void setNome(String nome){
        this.nome = nome;
    }

    public String getNome(){
        return this.nome;
    }

    public void setAnoNascimento(int anoNascimento){
        this.anoNascimento = anoNascimento;
    }

    public int getAnoNascimento(){
        return this.anoNascimento;
    }
    
    public void setAltura(double altura){
        this.altura = altura;
    }

    public double getAltura(){
        return this.altura;
    }

    public void setCpf(int cpf){
        this.cpf = cpf;
    }

    public int getCpf(){
        return this.cpf;
    }
    
        public Pessoa(){
            this.nome = "";
            this.altura = 0.0;
            this.anoNascimento = 0; 
            this.cpf = 0;       
            // this.peso = 0.0;
        }
    
        public Pessoa(double altura, double peso, int anoNascimento, String nome){
            this.altura = altura;
            this.anoNascimento = anoNascimento;
            this.nome = nome;
            // this.peso = peso;
        }
        
        public double calcularIMC(){
            return this.peso / (this.altura * this.altura);
        }
        
    public int idadeAtual(){
        return 2024 - anoNascimento;
    }

    public double aguaRecomendada(){
        return 0.035 * peso;
    }
    
    public void MostrarAtvAtributosEMetodosPt1(){
        System.out.printf("IMC: %.2f%n", calcularIMC());
        System.out.printf("Agua Recomendada: %.2f litros%n", aguaRecomendada());
        System.out.println("Idade Atual: " + idadeAtual() + " anos");
    }
    
    public void MostrarAtvConstrutoresEEncapsulamentoPt1(){
        System.out.println(getNome());
        System.out.println(getAltura());
        System.out.println(getAnoNascimento());
        System.out.println(getCpf());
    }

}