public class Invoice {
    private String identificadorItem;
    private int quantidadeCompradaItem;
    private double precoItem;

    public Invoice(int quantidadeCompradaItem, double precoItem, String identificadorItem){
        this.identificadorItem = identificadorItem;
        if(precoItem < 0){
            this.precoItem = 0;
        } else {
            this.precoItem = precoItem;     
        }
        if(quantidadeCompradaItem < 0){
            this.quantidadeCompradaItem = 0;
        } else {
            this.quantidadeCompradaItem = quantidadeCompradaItem;
        }
    }

    public void setIdentificadorItem(String identificadorItem){
        this.identificadorItem = identificadorItem;
    }

    public void setQuantidadeCompradaItem(int quantidadeCompradaItem){
        this.quantidadeCompradaItem = quantidadeCompradaItem;
    }

    public void setPrecoItem(double precoItem){
        this.precoItem = precoItem;
    }

    public String getIdentificadorItem(){
        return this.identificadorItem;
    }

    public int getQuantidadeCompradaItem(){
        return this.quantidadeCompradaItem;
    }

    public double getPrecoitem(){
        return this.precoItem;
    }

    public void calcularValorFatura(){
        System.out.println("Identificador: "+ getIdentificadorItem());
        System.out.println("Valor quantidade de items: "+ getQuantidadeCompradaItem());
        System.out.println("Preco do item: " + getPrecoitem());
        System.out.println("Valor da fatura: "+ getPrecoitem() * getQuantidadeCompradaItem());

    }

}
