public class Produto {
    private double peso;
    private double preco;
    private Produto[] produtos; // Array de produtos dentro da classe Produto

    // Construtor vazio
    public Produto() {
    }

    // Construtor para cada produto
    public Produto(double peso, double preco) {
        this.peso = peso;
        this.preco = preco;
    }

    // Método para inicializar o array com tamanho N
    public void criarArray(int n) {
        produtos = new Produto[n];
    }

    // Método para adicionar um produto no array
    public void adicionarProduto(int index, double peso, double preco) {
        if (index >= 0 && index < produtos.length) {
            produtos[index] = new Produto(peso, preco);
        } else {
            System.out.println("Índice fora do limite do array.");
        }
    }

    // Método para exibir os produtos cadastrados
    public void exibirProdutos() {
        if (produtos != null) {
            for (int i = 0; i < produtos.length; i++) {
                if (produtos[i] != null) {
                    System.out.println("Produto " + (i + 1) + " - Peso: " + produtos[i].getPeso() + ", Preço: " + produtos[i].getPreco());
                } else {
                    System.out.println("Produto " + (i + 1) + " - Não cadastrado.");
                }
            }
        }
    }

    // Método para contar produtos com peso > 10 e preço < 50
    public void contarProdutosPesoMaiorQue10PrecoMenorQue50() {
        int count = 0;
        if (produtos != null) {
            for (Produto p : produtos) {
                if (p != null && p.getPeso() > 10 && p.getPreco() < 50) {
                    count++;
                }
            }
        }
        System.out.println(count);
        }

    // Getters
    public double getPeso() {
        return peso;
    }

    public double getPreco() {
        return preco;
    }
}
