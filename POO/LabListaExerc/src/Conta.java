public class Conta {
    public int cpf;
    public int numConta;
    public int saldo;
    public boolean clienteEspecial;

    public double bonificacao(){
        if (clienteEspecial) {
            return this.saldo * 0.10;
        }else {
            return this.saldo * 0.05;
        }
    }

    public double saque(double retiraQuanto){
        if (saldo < retiraQuanto){
            System.out.println("Calma la, vc nao tem isso tudo nao");
            return saldo;
        } 
        else if (retiraQuanto == 0){
            System.out.println("Nao pode sacar 0");
            return saldo;
        }
        else{
            System.out.println("Aprovado capitao");
            return this.saldo -= retiraQuanto;
        }
    }
    
    public double deposito(double colocaQuanto){
        return this.saldo + colocaQuanto;
    }

    public void mostraAtributos(){
        System.out.println(this.saldo);
        System.out.println(this.cpf);
        System.out.println(this.numConta);
        System.out.println(bonificacao());
    }

    public void transferencia(Conta destinatario, double transfereQuanto){
        if (this.saldo < transfereQuanto){
            System.out.println("Calma la, vc nao tem isso tudo nao");
        } 
        else if (transfereQuanto == 0){
            System.out.println("Nao pode transferir 0");
        }
        else{
            this.saque(transfereQuanto);
            destinatario.deposito(transfereQuanto);
            System.out.println("Transferencia de " + transfereQuanto + " realizada para conta " + destinatario.numConta);
        }
    }
}
