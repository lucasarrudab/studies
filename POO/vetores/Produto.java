public class Produto {
    String codigo;
    double peso;
    double valor;

    public Produto(String codigo, double valor, double peso){
        this.codigo = codigo;
        this.valor = valor;
        this.peso = peso;
    }

    public void mostrarInfoProduto(Produto unidade){
        System.out.println(unidade.codigo + unidade.peso + unidade.valor);
    }

}
